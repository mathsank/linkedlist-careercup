#include<iostream>
#include<stack>
#include "llist.h"

using namespace std;

Llist:: Llist()
{ head = NULL;
    //sorted = NULL;
}
struct Node* Llist:: getHead()
{ return head;
}
void Llist:: setHead(struct Node* headV)
{ head = headV;
}
void Llist:: display()
{
    struct Node *temp;
    if(head == NULL)
    {
        cout<<"The List is Empty"<<endl;
        return;
    }
    temp = head;
    cout<<"Elements of list are: "<<endl;
    while (temp != NULL)
    {
        cout<<temp->data<<"->";
        temp = temp->next;
    }
    cout<<"NULL"<<endl;
}
Node* Llist:: createNode(int value)
{
    struct Node *temp;
    temp = new(struct Node);
    if (temp == NULL)
    {
        cout<<"Memory not allocated "<<endl;
        return 0;
    }
    else
    {
        temp->data = value;
        temp->next = NULL;
        return temp;
    }
}
void Llist:: insertBegining(int value)
{
    struct Node *temp;
    temp = createNode(value);
    struct Node* top;
    if (head == NULL)
    {
        head = temp;
        head->next = NULL;
    }
    else
    {
        top = head;
        head = temp;
        head->next = top;
    }
    cout<<"Element Inserted at beginning ";
}
int Llist:: length()
{
    int count = 0;
    struct Node *current = head;
    while (current != NULL)
    {
        count++;
        current = current->next;
    }
    return count;
}

void Llist:: removeDuplicates()
{
    struct Node *ptrI, *ptrJ, *dup;
    ptrI = getHead();
    
    //loop runs for each element
    while (ptrI != NULL && ptrI->next != NULL)
    {
        ptrJ = ptrI;
        
        //comparing each node with the remaining nodes
        while (ptrJ->next != NULL)
        {
            //delete if duplicate found
            if (ptrI->data == ptrJ->next->data)
            {
                
                dup = ptrJ->next;
                ptrJ->next = ptrJ->next->next;
                delete(dup);
            }
            else                        //else advance to next node to compare
               ptrJ = ptrJ->next;
        }
            ptrI = ptrI->next;              //pick next node
    }

}

Node* Llist:: kthToLast(int k)
{
    Node* ptrI = head;
    Node* ptrJ = head;
    
    for(int i=0;i<k;i++)            //advance ptrI k elements forward
    {
        if(ptrI == NULL)            //Outside boundary
            return NULL;
        ptrI = ptrI -> next;
    }
    
    while(ptrI!=NULL)               //when ptrI reaches end, ptrJ will point to kth element from end
    {
        ptrI = ptrI -> next;
        ptrJ = ptrJ -> next;
    }
    return ptrJ;
}

Node* Llist:: getNode(int pos)
{
    Node* current=head;
    int len = length();
    if(pos>len)
        return NULL;
    for(int i=0;i<pos;i++)
    {
        current = current->next;
    }
    
    return current;
}

void Llist:: deleteNode(struct Node* node)
{
    if(node==NULL||node->next == NULL)
        return;
    
    Node* temp = node->next;
    node->data = temp->data;
    node->next = temp->next;
}

Node* Llist:: partition(struct Node* headR, int val)
{
    //three lists keep track of smaller, equal and greater values to x
    struct Node *smallH = NULL, *smallT = NULL;             //head and tail for small,equal and great
    struct Node *greatT = NULL, *greatH = NULL;
    struct Node *equalH = NULL, *equalT = NULL;
    struct Node* current = headR;
    // move through list
    while (current != NULL)
    {
        // if node is equal to val then append it to equal list
        if (current->data == val)
        {
            if (equalH == NULL)
                equalH = equalT = current;
            else
            {
                equalT->next = current;
                equalT = equalT->next;
            }
        }
        
        // if node is less than val then append to small list
        else if (current->data < val)
        {
            if (smallH == NULL)
                smallT = smallH = current;
            else
            {
                smallT->next = current;
                smallT = current;
                
            }
        }
        else //if node is greater than val then append to great list
        {
            if (greatH == NULL)
                greatT = greatH = current;
            else
            {
                greatT->next  = current;
                greatT = current;
            }
        }
        
        current = current->next;
    }
    
    // if greater list has values make its end as null
    if (greatT!= NULL)
        greatT->next = NULL;
    
    
    
    //if smaller list is empty
    if (smallH == NULL)
    {
        if (equalH == NULL)         //if equal list is also empty then return great list
            return greatH;
        equalT->next = greatH;
        return equalH;
    }
    
    //if smaller list is not empty and equal list is empty then attach small list to great list and return
    if (equalH == NULL)
    {
        smallT->next = greatH;
        return smallH;
    }
    
    // if both small and equal list are not empty then append both together with great list and return
    smallT->next = equalH;
    equalT->next = greatH;
    return smallH;
    
}

Node* Llist:: addTwoLists (struct Node* first, struct Node* second)
{
    struct Node* res = NULL; // res is head node of the resultant list
    struct Node *temp, *prev = NULL;
    int carry = 0, sum;
    
    while (first != NULL || second != NULL) //while both lists exist
    {
        // sum is got by adding each digit of first second and carry
        sum = carry + (first? first->data: 0) + (second? second->data: 0);
        
        //update carry
        carry = (sum >= 10)? 1 : 0;
        
        //update sum for single digit
        sum = sum % 10;
        
        //create new node for this digit
        temp = createNode(sum);
        
        //if this is the first node then set it as head of result
        if(res == NULL)
            res = temp;
        else //if not then connect it to the rest.
            prev->next = temp;
        
        // update prev
        prev  = temp;
        
        // Move first and second pointers to next nodes
        if (first)
            first = first->next;
        if (second)
            second = second->next;
    }
    
    if (carry > 0)
        temp->next = createNode(carry);
    
    // return head of the resultant list
    return res;
}
bool Llist:: isPalindrome()
{
    struct Node* fast = head;
    struct Node *slow = head;
    
    stack<int> stack1;
    
    //push first half elements onto stack till fast pointer reaches end
    
    while(fast!=NULL && fast->next!=NULL)
    {
        stack1.push(slow->data);
        slow = slow->next;
        fast = fast->next->next;
    }
    
    if(fast!=NULL)                  //odd number of elements
        slow = slow->next;
    
    while(slow!=NULL)
    {
        int top = stack1.top();
        stack1.pop();
        if(top!=slow->data)
            return false;
        slow = slow->next;
    }
    return true;
}
int main()
{
    Llist list1,list2,rList;
    struct Node* temp,*head1,*head2,*headR;
    int k,n,x,pos,ch=1,choice,length=0,counter=0,val=0;
    
    while (1)
    {
        cout<<endl<<"---------------------------------"<<endl;
        cout<<endl<<"Operations on singly linked list"<<endl;
        cout<<endl<<"---------------------------------"<<endl;
        cout<<"1.Insert Node at beginning"<<endl;
        cout<<"2.Length of list"<<endl;
        cout<<"3.Remove Duplicates"<<endl;
        cout<<"4.Display linked list"<<endl;
        cout<<"5.Display kth element from the end"<<endl;
        cout<<"6.Delete some node in the middle"<<endl;
        cout<<"7.Partition"<<endl;
        cout<<"8.Add two lists"<<endl;
        cout<<"9.Palindrome"<<endl;
        cout<<"Enter your choice : ";
        cin>>choice;
        
        switch(choice)
        {
            case 1:
                cout<<"Enter value to be inserted: ";
                cin>>val;
                cout<<"Enter list number: ";
                cin>>ch;
                
                if(ch==1)
                    list1.insertBegining(val);
                else
                    list2.insertBegining(val);
                cout<<"of list "<<ch<<endl;
                break;
            case 2:
                cout<<"Enter list number: ";
                cin>>ch;
                
                if(ch==1)
                    length = list1.length();
                else
                    length = list2.length();
                cout<<"Length of list "<<ch<<" is "<<length<<endl;
                break;
                
            case 3:
                cout<<"Enter list number: ";
                cin>>ch;
                
                if(ch==1)
                    list1.removeDuplicates();
                else
                    list2.removeDuplicates();
                break;
            case 4:
                cout<<"Enter list number: ";
                cin>>ch;
                
                if(ch==1)
                    list1.display();
                else
                    list2.display();
                break;
            case 5:
                cout<<"Enter list number: ";
                cin>>ch;
                cout<<"Enter value for k: ";
                cin>>k;
                if(ch==1)
                    temp = list1.kthToLast(k);
                else
                    temp = list2.kthToLast(k);
                cout<<"Data: "<<temp->data<<endl;
                break;
            case 6:
                cout<<"Enter list number: ";
                cin>>ch;
                cout<<"Enter position of node to delete: ";
                cin>>k;
                
                if(ch==1)
                    { temp = list1.getNode(k);
                      list1.deleteNode(temp);
                      list1.display();
                    }
                else
                    { temp = list2.getNode(k);
                      list2.deleteNode(temp);
                      list2.display();
                    }
                break;
            case 7:
                cout<<"Enter list number: ";
                cin>>ch;
                cout<<"Enter value for partition: ";
                cin>>k;
                head1 = list1.getHead();
                head2 = list2.getHead();
                if(ch==1)
                    {   temp = list1.partition(head1,k);
                        list1.setHead(temp);
                        list1.display();
                    }
                else
                    {  temp = list2.partition(head2,k);
                       list2.setHead(temp);
                       list2.display();
                    }
                break;
            case 8:
                head1 = list1.getHead();
                head2 = list2.getHead();
                headR = list1.addTwoLists(head1,head2);
                rList.setHead(headR);
                rList.display();
                break;
            case 9:
                cout<<"Enter list number: ";
                cin>>ch;
                if(ch==1)
                    list1.isPalindrome()?cout<<"Yes"<<endl:cout<<"No"<<endl;
                else
                    list2.isPalindrome()?cout<<"Yes"<<endl:cout<<"No"<<endl;
                break;
            case 10:
                cout<<"Exiting..."<<endl;
                exit(1);
                break;
            default:
                cout<<"Wrong choice"<<endl;
        }
    }
return 0;
    
}
                
