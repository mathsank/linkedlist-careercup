struct Node
{
    int data;
    struct Node *next;
};

class Llist
{       struct Node *head;
        public:
                Llist();
                void setHead(struct Node* headV);
                struct Node* getHead();
                void display();
                Node* createNode(int value);
                void insertBegining(int value);
                int length();
                void removeDuplicates();
                Node* kthToLast(int k);
                Node* getNode(int pos);
                void deleteNode(struct Node* node);
                Node* partition(struct Node* head, int val);
                Node* addTwoLists (struct Node* first, struct Node* second);
                bool isPalindrome();
    
};
